const convert = require('../helpers/convert')
const assert = require('assert')


describe('Testing convert', () => {
  it('... toInt success ', () => {
    let value = '1'
    let number = convert.toInt(value) || undefined
    assert.notEqual(number, undefined, 'Ohh no Error!')
  })

  it('... toInt fail ', () => {
    let value = 'Vinicius'
    let number = convert.toInt(value) || undefined
    assert.equal(number, undefined, 'Ohh no Error!')
  });
})